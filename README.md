# Sentiment analysis for tweets, per trend. #

### What is this repository for? ###

This repository implements:

* sentiment analysis on tweets per trend
* formatting of the results by time

It is the 2nd of a series of 3 repositories, that together form a web application that visualizes sentiment information about twitter trends over time.

### How do I get set up? ###

All you need is:

* Java 7 (or later)
* MongoDB

### How to run it ? ###

1. Start a mongod
2. Run phase 1 (implemented in this repository: https://bitbucket.org/John_Halkida/twitteremotion/overview)
>This is the gathering phase.
>In this phase, two main operations are performed.
>
>1)The top 10 twitter trends are fetched and stored every 5 minutes.
>
>2)Tweets referring to these trends, are also being collected (even 2 hours after the last appearance of the trend).

3. Run the two functions of Main class.

### Full project (repositories overview) ###

1. Phase 1 (gather data)       :
> https://bitbucket.org/John_Halkida/twitteremotion/overview)
2. Phase 2 and 3 (process data):
> this repository
3. Phase 4 (visualize results)                    :
>https://bitbucket.org/alkoclick/twitteremotionwebapp/src/2aa71bec1ef380df325621beaad07ae4346e3fd1?at=master