package codeMerger;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.StringTokenizer;
import java.util.TimeZone;

import com.mongodb.MongoClient;
import com.mongodb.client.DistinctIterable;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.IndexOptions;
import com.mongodb.client.model.UpdateOptions;
import com.mongodb.client.result.UpdateResult;

import twitter4j.JSONObject;
import org.bson.Document;
import org.bson.types.ObjectId;

import com.mongodb.Block;
import static com.mongodb.client.model.Filters.*;

public class MongoHandler {
	private MongoDatabase db;
	private MongoClient mongoClient;
	HashMap<String,Integer> months;
	private MongoCursor<Document> tweets;
	
	private ArrayList<String> trends; 
	
	/**
	 * Creates a mongoClient object and connects to the local twitter db
	 */
	public MongoHandler(){
		
		mongoClient = new MongoClient();
		db = mongoClient.getDatabase("twitter");
		
		trends  = new ArrayList<String>();
		
		months = new HashMap<String,Integer>();
		months.put("Jan", 0);
		months.put("Feb", 1);
		months.put("Mar", 2);
		months.put("Apr", 3);
		months.put("May", 4);
		months.put("Jun", 5);
		months.put("Jul", 6);
		months.put("Aug", 7);
		months.put("Sep", 8);
		months.put("Oct", 9);
		months.put("Nov", 10);
		months.put("Dec", 11);
		
	}
	
	/**
	 * Returns the Date this tweet was posted
	 * */
	public Date getDateTweeted(TweetObject tweet){
		String dateStr =  db.getCollection(tweet.trend()).find(new Document("_id",tweet.id())).limit(1).first().getString("createdAt");
		
		return stringToDate(dateStr);
	}
	
	/**
	 * An abstract and simple representation of a tweet.
	 * */
	public class TweetObject{
		private String text;
		private ObjectId id;
		private String trend;
		
		public TweetObject(ObjectId id1,String text1,String trend1){
			text = text1;
			id = id1;
			trend = trend1;
		}
		
		public String text(){return text;}
		public ObjectId id(){return id;}
		public String trend(){return trend;}
	}
	
	/**
	 * A simple representation of an active period. The only thing we need to know about it is time it started and the time it ended.
	 * */
	public class ActivePeriod{
		private Date start;
		private Date end;
		
		public ActivePeriod(Date s, Date e){
			start = s;
			end   = e;
		}
		
		public Date start(){return start;}
		public Date end(){return end;}
	}
	
	/**
	 * A representation of the results of the sentiment analysis on tweets of a specific trend that have been posted
	 * over a period of 5 minutes.
	 * */
	public class Minutes5{
		
		private double[] scores;
		private ArrayList<String> tags;
		private Date start;
		
		public Minutes5(double[] s, ArrayList<String> t, Date d){
			
			scores = s;
			tags = t;
			start = d;
		}
		
		public double[] getScores(){return scores;}
		public ArrayList<String> getTags(){return tags;}
		public Date getStartDate(){return start;}
	}
	
	/**
	 * Returns the date (DATE FOR MONGO) and time of param "myDate" in EET Timezone as a "java.util.Date" object.
	 * */
	public Date getDateInEET(Date myDate){
		
		String DATEFORMAT = "yyyy-MM-dd HH:mm:ss";
		SimpleDateFormat sdf = new SimpleDateFormat(DATEFORMAT);
	    sdf.setTimeZone(TimeZone.getTimeZone("EET"));
	    String utcTime = sdf.format(myDate);
	    
	    SimpleDateFormat dateFormat = new SimpleDateFormat(DATEFORMAT);
	    Date dateToReturn=null;
	    
	    try
	    {
	        dateToReturn = (Date)dateFormat.parse(utcTime);
	    }
	    catch (ParseException e)
	    {
	    	System.out.println("Date parsing Exception.");
	        e.printStackTrace();
	    }
	    return dateToReturn;

	}
	
	/**
	 * Parses a given String date of the format: "Wed Jan 20 20:48:18 EET 2016" and returns a Date object in EET.
	 * */
	public Date stringToDate(String dateString){
		
		Calendar cal = Calendar.getInstance();
		
		StringTokenizer tok = new StringTokenizer(dateString,":,EET, ");
		
		tok.nextToken();//Dump day name
		int month  = months.get(tok.nextElement());
		int dayNum = Integer.valueOf(tok.nextToken());
		int hour   = Integer.valueOf(tok.nextToken());
		int min    = Integer.valueOf(tok.nextToken());
		int sec    = Integer.valueOf(tok.nextToken());
		int year   = Integer.valueOf(tok.nextToken());
		
		cal.set(year, month, dayNum, hour, min, sec);

		return  cal.getTime();
	}
	
	/**
	 * Stores a Minutes5 object inside the collection named "trend"+"_res". Example for param trend="hello" 
	 * this function will insert a document inside the collection named "hello_res"
	 * */
	public void write5Minutes(Minutes5 min, String trend){
		
		double[] scores = min.getScores();
		
		db.getCollection(trend+"_res").insertOne(
				new Document("start",min.getStartDate())
				.append("tags", min.getTags())
				.append("anger", scores[0])
				.append("disgust", scores[1])
				.append("fear", scores[2])
				.append("joy", scores[3])
				.append("sadness", scores[4])
				.append("surprise", scores[5])
		);
	}
	
	public void writeTagsForTotalTime(String trend, ArrayList<String> tags){
		
		db.getCollection(trend+"_res").insertOne(new Document("tagsTotal",tags));
	}
	
	/**
	 * Returns an iterator to all active periods for a given trend's document inside collection "trendWords"
	 * */
	public Iterator<ActivePeriod> getActivePeriods(String trend){
		
		ArrayList<ActivePeriod> activePeriods = new ArrayList<ActivePeriod>();
		
		@SuppressWarnings("unchecked")
		ArrayList<Document> dates = (ArrayList<Document>)db.getCollection("trendWords").find(new Document("content",trend)).limit(1).projection(new Document("_id",0).append("datesList", 1)).first().get("datesList"); 
		Iterator<Document> periods = dates.iterator();
		while(periods.hasNext()){
			
			Document period = periods.next();
			ActivePeriod aperiod = new ActivePeriod(period.getDate("start"),period.getDate("end"));
			activePeriods.add(aperiod);
		}
		
		return activePeriods.iterator();
	}
	
	/**
	 * Adds a given tweetJson object to the given collection
	 * @param tweetJSON The tweet to be parsed to string and added
	 * @param collection The collection to put the tweet into
	 */
	public void addTweet(JSONObject tweetJSON,String collection){
		
			db.getCollection(collection).insertOne(Document.parse(tweetJSON.toString()));
	}
	
	/** If a trend with a "content" field equal to the content of the trend parameter exists,
	 * then push a new entry in it's "datesList" list and update it's "timeLastSeen" field.
	 * If such a trend does NOT exist then create one such document and perform the above operations.
	 * When this is done, if a new document was created during the previous operations then set it's "timeFirstSeen" field to a date, 
	 * if not then keep it's "timeFirstSeen" field untouched. */
	public void isNewOrRecurringOperation(TrendWord trend){
		
		Date now = new Date();
	    Date dateUTC = getDateInEET(now);//Mongo thinks that whatever Date we add is Local time and converts it from Local to UTC. So we add Local time in Greece (and it stores UTC that is 2hours behind)
				
		UpdateOptions uo = new UpdateOptions();
		uo.upsert(true);
		
		UpdateResult res = db.getCollection("trendWords").updateOne(
								
				new Document("content", trend.getContent())
				,
				new Document("$push",
					new Document("datesList",
							new Document("start",dateUTC)
							.append("end", dateUTC)
					)
				)
				.append("$set", new Document("timeLastSeen",dateUTC)),
				uo
		);
		
		//It was a NEW trend!
		if(res.getUpsertedId() != null){
			
			ArrayList<Document> datesList = new ArrayList<Document>();
			datesList.add(new Document("start",dateUTC)
								.append("end", dateUTC));
			
			db.getCollection("trendWords").findOneAndUpdate(
					
					new Document("content", trend.getContent()),
					new Document("$set",
							new Document("timeFirstSeen",dateUTC)
							.append("datesList", datesList)
					)
			);
		}
	}
	
	/** Finds a document of "trendWords" with "content"="DUMMY" and updates the latest "end" Date field inside it's "myList" list field */
	public void isActiveOperation(TrendWord trend){
		
		int activeHours = 2;
		
		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
		cal.roll(Calendar.HOUR_OF_DAY, -activeHours);
		
		Date now = new Date();
		Date past = cal.getTime();
		
		Date pastUTC = getDateInEET(past);
		Date nowUTC  = getDateInEET(now);
		
		db.getCollection("trendWords").findOneAndUpdate(
			
				new Document("content", trend.getContent())
					.append("datesList.end",
							new Document("$gte",pastUTC)
				),
				new Document("$set",
						new Document("datesList.$.end",nowUTC)
						.append("timeLastSeen", nowUTC)
				)
		);
		
	}
	
	/**
	 * Adds a trendWord to collection "trendWords".
	 * @param isActive : indicates that the document's "timeLastSeen" field is not more than 2 hours before than the current hour.
	 * */
	public void addTrendWord(TrendWord trend, boolean isActive){

		if(isActive){
			isActiveOperation(trend);
		}else{
			isNewOrRecurringOperation(trend);
		}
	}
	
	/**
	 * Create and add a dummy Trend object on a custom date
	 * @param content The content of the trendWord object
	 * @param dateFirstSeen
	 * @param dateLastSeen
	 */
	public void addDummy(String content, Date dateFirstSeen, Date dateLastSeen){
		db.getCollection("trendWords").insertOne(
				new Document("content",content)
				.append("timeFirstSeen", dateFirstSeen)
				.append("timeLastSeen", dateLastSeen)
				.append("myList", new ArrayList<Date>())
				);
	}
	
	/**
	 * Adds the screenName of a user to the "users_" collection that contains the screenNames of all users that have posted a tweet.
	 * If the name does not exist in the collection, it is inserted as a new Document, otherwise the screenName of 
	 * the Document is reset to it's current value. 
	 * */
	private void addUser(String screenName){
		
		UpdateOptions uo = new UpdateOptions();
		uo.upsert(true);
		
		db.getCollection("users_").updateOne(
								
				new Document("screenName", screenName),
				new Document("$set", new Document("screenName", screenName)),
				uo
		);
	}
	
	/**
	 * Starts creating two indexes for each "trend" collection that stores tweets.
	 * 1) An  index on "user.screenName" to support queries of the form SELECT * FROM collection WHERE user.screenName = "John Smith" which "getTweets" uses.
	 * 2) An  index on "createdAt" to support queries of the form SELECT * FROM COLLECTION WHERE createdAt>start AND createdAt < end  which "" uses.
	 * These indexes are created on background, which means that they are not used until their creation is completed.
	 * The indexes are B-tree like.
	 * @param collection : the name of the collection on which the two indexes are going do be built.
	 * */
	private void createIndexes(String collection){
		db.getCollection(collection).createIndex(new Document("user.screenName",1), new IndexOptions().background(true));
		//db.getCollection(collection).createIndex(new Document("createdAt",1), new IndexOptions().background(true));
		//Unfortunately a tweet's "createdAt" field is a String so a text index is useless ...
	}
	

	public boolean exists(String collection,ObjectId id){
		return db.getCollection(collection).find(eq("_id", id)).iterator().hasNext();
	}
	
	/**
	 * Shut down client gracefully
	 */
	public void closeClient() {
		mongoClient.close();
	}
	
	/**
	 * Reads the content of each document inside the collection "trendWords" and inserts it to an ArrayList<String> called "trends".
	 * Also, for each user that has posted a tweet, a Document is inserted in the "users_" collection.
	 * */
	public void fetchUsersAndTrends(){
		
		FindIterable<Document> iterableTrends = db.getCollection("trendWords").find().projection(new Document("content",1));
		MongoCursor<Document> cursor = iterableTrends.iterator();
		while(cursor.hasNext()){
			String content = cursor.next().getString("content");
			trends.add(content);
			
			createIndexes(content);
			
			DistinctIterable<String> iterableTweets = db.getCollection(content).distinct("user.screenName",String.class);
			MongoCursor<String> cursor2 = iterableTweets.iterator();
			while(cursor2.hasNext()){
				String screenName = cursor2.next();
				
				addUser(screenName);
			}
		}
	}
	
	/**
	 * Sets an iterator at the first document inside the collection named as the parameter and returns it.
	 * If fetchUsersAndTrends() has been called, then there is no need to call this function.
	 * If not, then this function is much faster than fetchUsersAndTrends() for getting the trends names.
	 * */
	public MongoCursor<Document> getTrendsIterator(String collection){
		
		return db.getCollection("trendWords").find().projection(new Document("content",1)).noCursorTimeout(true).iterator();
		//return db.getCollection("trendWords").find().projection(new Document("content",1)).iterator();
	}
	
	/**
	 * Returns an iterator to all Minutes5 documents of a collection named as the trend param and a "_res" at the end.
	 * Example: if param trend="hello" then the collection is "hello_res"
	 * */
	public MongoCursor<Document> getMinutes5(String trend){
		
		return db.getCollection(trend+"_res").find(new Document("tags",new Document("$exists",true))).iterator();
	}
	
	/**
	 * 
	 * */
	public ArrayList<String> getTotalTags(String trend){
		
		Document doc = db.getCollection(trend+"_res").find(new Document("tagsTotal",new Document("$exists",true))).limit(1).first();
		@SuppressWarnings("unchecked")
		ArrayList<String> totalTags = (ArrayList<String>)doc.get("tagsTotal");
		return totalTags;
	}
	
	/**
	 * Removes the tweet with the given id from the given collection
	 * @param id : the id of the tweet inside the collection
	 * @param collection : the name of the collection in which the tweet belongs
p	 * */
	public void removeTweet(ObjectId id, String collection){
		//db.getCollection(collection).deleteOne(new Document("_id",id));
		db.getCollection(collection).updateOne(new Document("_id",id), new Document("$set",new Document("deleted",1)));
	}
	
	/**
	 * Appends some fields that represent the sentimental scores inside the Document of the tweet with the given id.
	 * These fields are "anger","disgust","fear","joy","sadness","surprise"
	 * @param id : the id of the tweet inside the collection
	 * @param collection : the name of the collection in which the tweet belongs
	 * */
	public void appendScoresToTweet(ObjectId id, String collection, double[] score){
		
		db.getCollection(collection).updateOne(new Document("_id",id), new Document("$set",
				new Document("anger",score[0])
				.append("disgust", score[1])
				.append("fear", score[2])
				.append("joy", score[3])
				.append("sadness", score[4])
				.append("surprise", score[5])
		));
		
	}
	
	/**
	 * Returns an iterator Documents that contain one field, the screenName of each user that has posted a tweet.
	 * */
	public MongoCursor<Document> getUsers(){
		return db.getCollection("users_").find().projection(new Document("screenName",1).append("_id", 0)).noCursorTimeout(true).iterator();
		//return db.getCollection("users_").find().projection(new Document("screenName",1).append("_id", 0)).iterator();
	}
	
	/**Returns all trends inside the collection "trendWords"*/
	public ArrayList<String> getTrends(){return trends;}
	
	/**
	 * Returns the text of all tweets posted by a specific user. 
	 * */
	public ArrayList<TweetObject> getTweets(String user){
		
		ArrayList<TweetObject> tweets = new ArrayList<TweetObject>();
		
		Iterator<String> it = trends.iterator();
		while(it.hasNext()){
			String trend = it.next();
			
			FindIterable<Document> tweetsIterable = db.getCollection(trend).find(eq("user.screenName", user)).projection(new Document("text",1).append("_id", 1));
			MongoCursor<Document> cursor = tweetsIterable.iterator();
			while(cursor.hasNext()){
				
				Document doc = cursor.next();
				String tweet = doc.getString("text");
				ObjectId id = doc.getObjectId("_id");
				
				//System.out.println("id:"+id+" ->  "+tweet + " was posted on "+trend +" (id exists:"+exists(trend, id)+")");
				
				tweets.add(new TweetObject(id,tweet,trend));
			}
		}
		
		return tweets;
	}
	
	/**
	 * Sets an iterator at the first tweet of the collection named "trend"
	 * */
	public void initializeTweetIterator(String trend){
		
		tweets = db.getCollection(trend).find()
				.projection(
						new Document("_id",0)
						.append("createdAt", 1)
						.append("text", 1)
						.append("anger",1)
						.append("disgust", 1)
						.append("fear", 1)
						.append("joy", 1)
						.append("sadness", 1)
						.append("surprise", 1)
						
				).iterator();
		
	}
	
	public void closeCursor(){tweets.close();}
	
	/**
	 * Returns a Document that contains all emotions and the text of the next tweet that belongs to the given active period or null. 
	 * */
	public Document nextTweet(String trend,ActivePeriod period){
				
		if(tweets.hasNext()){
			
			Document doc = tweets.next();
			Date createdAt = stringToDate(doc.getString("createdAt"));
			
			//period.end() is the Date we last saw this trend. This trend kept being active for 2 hours after this Date.

			if( (createdAt.getTime() >= period.start().getTime()) && (createdAt.getTime() <= (period.end().getTime()+(7200*1000))) ){
				
				return doc;
			}else{
				tweets.close();
				return null;//After the period
			}
			
		}else{
			tweets.close();
			return null;//No more tweets
		}
	}
	
	/**
	 * Drops all collections whose name ends is "_res" that are collections,
	 * each document of which, contain sentiments and tags for a period of 5minutes
	 * */
	public void dropResCollections(){
		
		MongoCursor<Document> collections = getTrendsIterator("trendWords");
		while(collections.hasNext()){
			
			String trend = collections.next().getString("content");
			db.getCollection(trend+"_res").drop();
		}
	}
	
	/**
	 * Drops a collection named "users_"
	 * */
	public void dropUsersCollection(){
		
		db.getCollection("users_").drop();
	}
	
	/**
	 * Prints a given collection
	 * @param collection
	 */
	public void printTrendWords(String trend) {
		FindIterable<Document> iterable = db.getCollection("trendWords").find(eq("content", trend));
		iterable.forEach(new Block<Document>() {
		    @Override
		    public void apply(final Document document) {
				try {
					//System.out.println(new JSONObject(document.toJson()));
					System.out.println("content:"+document.getString("content") +
							"\ntimeFirstSeen:"+document.getDate("timeFirstSeen") +
							"\ntimeLastSeen:"+document.getDate("timeLastSeen")+
							"\ndatesList: [");
					
					@SuppressWarnings("unchecked")
					ArrayList<Document> dates = (ArrayList<Document>) document.get("datesList");
					Iterator<Document> it = dates.iterator();
					while(it.hasNext()){
						
						Document doc = it.next();
						Date start = doc.getDate("start");
						Date end = doc.getDate("end");
						
						System.out.println("\t"+"start:"+start+" , end:"+end);
					}
					System.out.println("]");
				//} catch (JSONException e) {
				} catch (Exception e) {
					e.printStackTrace();
				}
		    }
		});
	}
}