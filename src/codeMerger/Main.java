package codeMerger;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;


import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCursor;

import SentimentAnalysis.WordNetHandler;
import codeMerger.MongoHandler.ActivePeriod;
import codeMerger.MongoHandler.Minutes5;
import codeMerger.MongoHandler.TweetObject;
import twitter4j.JSONException;
import twitter4j.JSONObject;

public class Main {
	
	public static void main(String[] args) {		
				
		//MongoClient mongoClient = new MongoClient();
		//mongoClient.dropDatabase("twitter4");
		//mongoClient.close();
		codeMerger.extractSentimentsAndEliminateDuplicates();
	    codeMerger.prepareInputForWebApp();	
		
		//Example of how to use sentiment extractor
		//********************************************
		//String testTweet1 = "I am too Disappointed, furious and sad at the same time";
		//String testTweet2 = "I feel really happy and joyful today";
		//SentimentInterface test = new SentimentInterface();
		//double[] scores1 = test.extractSentiments(testTweet1);// scores1[0] contains score for anger, scores1[1] contains score for disgust, scores1[2] contains score for fear, scores1[3] contains score for joy, scores1[4] contains score for sadness, scores1[5] contains score for surprise. 
		//double[] scores2 = test.extractSentiments(testTweet2);// the same as above.
		//**********************************************
		
//		Hint: To remove comments from a line (or a block), use Ctrl+Shift+C! (Eclipse)

		

////Drop twitter3 --------------------------------------------------------------------------------------------------------
//	
//		MongoClient mongoClient = new MongoClient();
//		mongoClient.dropDatabase("twitter3");
//		mongoClient.close();	
//		
//////Create the collections----------------------------------------------------------------------------------
//		
//		MongoHandler mongo = new MongoHandler();
//		mongo.dropResCollections();
//		TrendWord t1 = new TrendWord("trend1",null);
//		TrendWord t2 = new TrendWord("trend2",null);
//		TrendWord t3 = new TrendWord("trend3",null);
//		TrendWord t4 = new TrendWord("trend4",null);
//		TrendWord t5 = new TrendWord("trend5",null);
//		
//		mongo.addTrendWord(t1,false);
//		mongo.addTrendWord(t2,false);
//		mongo.addTrendWord(t3,false);
//		mongo.addTrendWord(t4,false);
//		mongo.addTrendWord(t5,false);
//	
//////Add some tweets fot user1 ------------------------------------------------------------------------------
//		
//		Calendar cal1 = Calendar.getInstance();
//		cal1.roll(Calendar.MINUTE, 1);
//		Date d1 = cal1.getTime();
//		
//		String user = "Dave Brubeck";
//		for(int i=0;i<10;i++){
//		
//			JSONObject tweet = new JSONObject();
//			try {
//				tweet.put("text", "i'm a pianist "+i);
//				tweet.put("user", new JSONObject().put("screenName",user).put("country", "Norway"));
//				tweet.put("createdAt", d1);
//			} catch (JSONException e){}
//			
//			mongo.addTweet(tweet, "trend"+((i%5)+1));
//		}
//		
//		Calendar cal = Calendar.getInstance();
//		cal.roll(Calendar.HOUR_OF_DAY, 3);
//		Date d = cal.getTime();
//		
//		for(int i=11;i<20;i++){
//			
//			JSONObject tweet = new JSONObject();
//			try {
//				tweet.put("text", "i'm a pianist "+i);
//				tweet.put("user", new JSONObject().put("screenName",user).put("country", "Norway"));
//				tweet.put("createdAt", d);
//			} catch (JSONException e){}
//			
//			mongo.addTweet(tweet, "trend"+((i%5)+1));
//		}
//		
//		
////Add some tweets for user2 -----------------------------------------------------------------------------
//		
//		user = "Paul Desmond";
//		for(int i=0;i<10;i++){
//		
//			JSONObject tweet = new JSONObject();
//			try {
//				tweet.put("text", "i'm a saxophonist "+i);
//				tweet.put("user", new JSONObject().put("screenName",user).put("country", "America"));
//				tweet.put("createdAt", d1);
//			} catch (JSONException e){}
//			
//			mongo.addTweet(tweet, "trend"+((i%5)+1));
//		}	

		
//Put some scores------------------------------------------------------------------------------------
		//MongoHandler mongo = new MongoHandler();

// 		mongo.fetchUsersAndTrends();
// 		MongoCursor<Document> users = mongo.getUsers();
//
// 		while(users.hasNext()){
//
// 			String u = users.next().getString("screenName");
//
// 			ArrayList<TweetObject> tweets = mongo.getTweets(u);
//
// 			double[] scores = {0.1, 0.2, 0.3, 0.4, 0.5, 0.6};
//
// 			//Print the tweets
// 			Iterator<TweetObject> it = tweets.iterator();
//
// 			while(it.hasNext()){
//
// 				TweetObject tweet = it.next();
//
// 				//mongo.removeTweet(tweet.id(), tweet.trend());
//				mongo.appendScoresToTweet(tweet.id(), tweet.trend(), scores);
//
// 				//System.out.println("id:"+tweet.id()+" , text:"+tweet.text());				
//				//System.out.println(mongo.getDateTweeted(tweet));
//
// 			}
//		}	
//--------------------------------------------------------------------------------

//WEB APP MONGO CALLS
//		MongoHandler mongo = new MongoHandler();
//		MongoCursor<Document> mins = mongo.getMinutes5("dummy");
//		while(mins.hasNext()){
//			
//			Document min = mins.next();
//			
//			@SuppressWarnings("unchecked")
//			ArrayList<String> tags = (ArrayList<String>)min.get("tags");
//			
//			System.out.println(
//					"start:"	+min.getDate("start")+
//					"anger:"	+min.getDouble("anger")+
//					"disgust:"	+min.getDouble("disgust")+
//					"fear:"		+min.getDouble("fear")+
//					"joy:"		+min.getDouble("joy")+
//					"sadness:"	+min.getDouble("sadness")+
//					"surprise:"	+min.getDouble("surprise")+
//					"tags:"	    +tags				
//					);
//		}
//		ArrayList<String> tagsTotal = mongo.getTotalTags("dummy");
//		System.out.println(tagsTotal);
//		Document d = new Document("bob",1);
//		System.out.println(d.containsKey("bob"));
//		System.out.println(d.containsKey("jack"));
		
		
//Get tweets for each trend, for each active period ------------------------------------------------------------------------------
		
//		MongoHandler mongo = new MongoHandler();
//		ArrayList<String> tags = new ArrayList<String>();
//		tags.add("piano");
//		tags.add("sax");
//		tags.add("trumpet");
//		tags.add("horn");
//		tags.add("tuba");
//		mongo.writeTagsForTotalTime("dummy", tags);		
		
//		MongoHandler mongo = new MongoHandler();
//		mongo.fetchUsersAndTrends();
//		
//		Iterator<String> trends = mongo.getTrends().iterator();
//		while(trends.hasNext()){
//			
//			String trend = trends.next();
//			System.out.println("TREND: "+trend+"\n###\n###\n###\n###");
//			Iterator<ActivePeriod> periods = mongo.getActivePeriods(trend);
//			while(periods.hasNext()){
//				
//				ActivePeriod period = periods.next();
//				System.out.println(trend+" --> "+period.start()+"  "+period.end());
//				
//				mongo.initializeTweetIterator(trend);
//				Document tweet = mongo.nextTweet(trend, period);
//				
//				while(tweet != null){
//					
//					System.out.println(tweet.getString("text"));
////					System.out.println("text:"       +  tweet.getString("text")+
////									   "\ncreatedAt:"+  mongo.stringToDate(tweet.getString("createdAt"))+
////									   "\nanger:"    +  tweet.getDouble("anger")+
////									   "\ndisgust:"  +  tweet.getDouble("disgust")+
////									   "\nfear:"     +  tweet.getDouble("fear")+
////									   "\njoy:"      +  tweet.getDouble("joy")+
////									   "\nsadness:"  +  tweet.getDouble("sadness")+
////									   "\nsurprise:" +  tweet.getDouble("surprise")
////					);
//
//					tweet = mongo.nextTweet(trend, period);
//				}
//			}
//		}
		
	}
}
