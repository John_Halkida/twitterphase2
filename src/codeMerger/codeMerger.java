package codeMerger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;

import org.bson.Document;

import com.mongodb.client.MongoCursor;

import SentimentAnalysis.LevensteinDistance;
import SentimentAnalysis.SentimentInterface;
import SentimentAnalysis.Tokenizer;
import SentimentAnalysis.WordNetHandler;
import codeMerger.MongoHandler.ActivePeriod;
import codeMerger.MongoHandler.Minutes5;
import codeMerger.MongoHandler.TweetObject;

public class codeMerger 
{
	
	private static Hashtable<String,Integer> finalAngerScore = new Hashtable<>();
	private static Hashtable<String,Integer> finalDisgustScore = new Hashtable<>();
	private static Hashtable<String,Integer> finalFearScore = new Hashtable<>();
	private static Hashtable<String,Integer> finalJoyScore = new Hashtable<>();
	private static Hashtable<String,Integer> finalSadnessScore = new Hashtable<>();
	private static Hashtable<String,Integer> finalSurpriseScore = new Hashtable<>();
	
	private static Hashtable<String,Integer> angerScore = new Hashtable<>();
	private static Hashtable<String,Integer> disgustScore = new Hashtable<>();
	private static Hashtable<String,Integer> fearScore = new Hashtable<>();
	private static Hashtable<String,Integer> joyScore = new Hashtable<>();
	private static Hashtable<String,Integer> sadnessScore = new Hashtable<>();
	private static Hashtable<String,Integer> surpriseScore = new Hashtable<>();
	
    public static void extractSentimentsAndEliminateDuplicates()
    {	
        MongoHandler mongo = new MongoHandler();
        mongo.fetchUsersAndTrends();// Fetch usernames.
		
		MongoCursor<Document> users = mongo.getUsers();// Create an iterator on all usernames.
		
		SentimentInterface sentimentExtractor = new SentimentInterface();// Call constructor for sentiment extractor object.
		
		while(users.hasNext())// while there are more users
		{
			String u = users.next().getString("screenName");// u is the name of the current user.
			
			//System.out.println("-------------------------------------------------- User: "+u+" -------------------------------");
			
			ArrayList<TweetObject> tweets = mongo.getTweets(u);// arraylist of all tweets for the current user.
			
			//Iterator<String> it = tweets.iterator();// iterator on all the tweets of the current user.
			
			int currentTweetId;// id of the current tweet (not the real id, a fake one to help the calculations).
			
			for(currentTweetId=0;currentTweetId<tweets.size();++currentTweetId)// while the tweetlist has more tweets
			{
				String text = tweets.get(currentTweetId).text();// text contains the body of the tweet.
				//System.out.println("Current tweet "+text);
				//System.out.println();
				double[] sentiments = sentimentExtractor.extractSentiments(text);// extract sentiments of the tweet.
				
				mongo.appendScoresToTweet(tweets.get(currentTweetId).id(),tweets.get(currentTweetId).trend(),sentiments);// save sentiments of tweet to the database.
				
				for(int i=currentTweetId+1;i<tweets.size();++i)// for every other tweet of the same user except the ones that come before it
				{
					int distance = LevensteinDistance.getLevensteinDistance(text, tweets.get(i).text());
					//System.out.println();
					//System.out.println(distance);
					//System.out.println();
					if(distance < 10)
					{
						//System.out.println("Duplicate tweet of user "+u+" with text "+tweets.get(i).text()+" deleted. (Distance was "+distance+")");
						//System.out.println();
						mongo.removeTweet(tweets.get(i).id(), tweets.get(i).trend());// delete duplicate tweet from mongo database.					
						tweets.remove(i);// remove the duplicate tweet from the arraylist.
						--i;// We reduce the counter by one because the arraylist positions shift to the left by one after removing a tweet.
					}
				}
			}
			//System.out.println("-----------------------------------------------------------------------------------");
		}
    	mongo.closeClient();
    	System.out.println("Phase 2 is done.");
    }
    
    public static void prepareInputForWebApp()
    {
    	double timeLimitInSeconds = 60.0*5.0;
    	MongoHandler mongo = new MongoHandler();

    	mongo.dropResCollections();
       
		MongoCursor<Document> trends = mongo.getTrendsIterator("trendWords");
    	
    	while(trends.hasNext()){

			String trend = trends.next().getString("content");
			//System.out.println("Trend "+trend);
			//System.out.println();

          	Iterator<ActivePeriod> periods = mongo.getActivePeriods(trend);
          	while(periods.hasNext())
    	    {
          		ActivePeriod period = periods.next();
          		//System.out.println("Period "+period.toString());
          		//System.out.println();
              	double[] sentimentScores = new double[6]; 
              	Arrays.fill(sentimentScores, 0.0);
              	mongo.initializeTweetIterator(trend);    	
				Document tweet = mongo.nextTweet(trend, period);
                if(tweet == null)
                {
                	System.out.println();
                	System.out.println("CRITICAL WARNING : Null tweet found. Trying to force next trend period to make this work. Thank me later.");
                	System.out.println();
                	break;
                }
				Date start_timestamp = mongo.stringToDate(tweet.getString("createdAt"));
				//System.out.println("-------------------------------------------------------------");
				while(tweet != null)
              	{
					//System.out.println(mongo.stringToDate(tweet.getString("createdAt")));
					if(tweet.containsKey("anger") && differenceInSeconds(mongo.stringToDate(tweet.getString("createdAt")),start_timestamp) < 0)// if the tweet has sentiments attached.
					{
						//System.out.println("Normal tweet "+tweet.getString("text"));
						double[] currentTweetSentiments = new double[6];
						String tweetText = tweet.getString("text");
						String[] tweetWords = tokenizeTweet(tweetText);
						tweetWords = WordNetHandler.removeOneLetterWords(tweetWords);
						tweetWords = WordNetHandler.removeStopWords(tweetWords);
						tweetWords = WordNetHandler.removeLinks(tweetWords);
						tweetWords = WordNetHandler.removeHttpAndwww(tweetWords);
						tweetWords = WordNetHandler.removeNumbers(tweetWords);
						tweetWords = WordNetHandler.removeMentions(tweetWords);
						currentTweetSentiments[0] = tweet.getDouble("anger");
						currentTweetSentiments[1] = tweet.getDouble("disgust");
						currentTweetSentiments[2] = tweet.getDouble("fear");
						currentTweetSentiments[3] = tweet.getDouble("joy");
						currentTweetSentiments[4] = tweet.getDouble("sadness");
						currentTweetSentiments[5] = tweet.getDouble("surprise");
						sentimentScores[0] = currentTweetSentiments[0] + sentimentScores[0];
						sentimentScores[1] = currentTweetSentiments[1] + sentimentScores[1];
						sentimentScores[2] = currentTweetSentiments[2] + sentimentScores[2];
						sentimentScores[3] = currentTweetSentiments[3] + sentimentScores[3];
						sentimentScores[4] = currentTweetSentiments[4] + sentimentScores[4];
						sentimentScores[5] = currentTweetSentiments[5] + sentimentScores[5];
						int currentTweetStrongestSentiment = findStrongestSentiment(currentTweetSentiments);// contains the array position with the strongest sentiment.
		              	for(int i=0;i<tweetWords.length;++i)
		              	{
		                    writeToHashTables(currentTweetStrongestSentiment,tweetWords[i]);
		              	}
	              		if (Math.abs(differenceInSeconds(mongo.stringToDate(tweet.getString("createdAt")),start_timestamp))> timeLimitInSeconds)
	              		{
	              			//System.out.println("saving five minute doc");
	              			int strongestSentiment = findStrongestSentiment(sentimentScores);
	                        ArrayList<String> fiveMinuteTags = findTopWords(getTopSentimentHashTable(strongestSentiment));
	              			Minutes5 results = mongo.new Minutes5(sentimentScores,fiveMinuteTags,start_timestamp);
	                        mongo.write5Minutes(results, trend);
	              			start_timestamp = mongo.stringToDate(tweet.getString("createdAt"));// change the start timestamp to that of the new tweet.
	              			angerScore.clear();
	              			disgustScore.clear();
	              			fearScore.clear();
	              			joyScore.clear();
	              			sadnessScore.clear();
	              			surpriseScore.clear();
	              			Arrays.fill(sentimentScores, 0.0);
	              		}
					}
					else
					{
						//System.out.println("Duplicate tweet "+tweet.getString("text"));
					}
              		tweet = mongo.nextTweet(trend, period);
              		if(tweet == null)// if there are no more tweets for the given period save the local hashtables.
              		{
              			//System.out.println("saving");
              			int strongestSentiment = findStrongestSentiment(sentimentScores);
              			ArrayList<String> fiveMinuteTags = findTopWords(getTopSentimentHashTable(strongestSentiment));
              			Minutes5 results = mongo.new Minutes5(sentimentScores,fiveMinuteTags,start_timestamp);
                        mongo.write5Minutes(results, trend);
              			angerScore.clear();
              			disgustScore.clear();
              			fearScore.clear();
              			joyScore.clear();
              			sadnessScore.clear();
              			surpriseScore.clear();
              		}
              	}
    	    }
            ArrayList<String> angerWords = findTopWords(finalAngerScore);
            ArrayList<String> disgustWords = findTopWords(finalDisgustScore);
            ArrayList<String> fearWords = findTopWords(finalFearScore);
            ArrayList<String> joyWords = findTopWords(finalJoyScore);
            ArrayList<String> sadnessWords = findTopWords(finalSadnessScore);
            ArrayList<String> surpriseWords = findTopWords(finalSurpriseScore);
            ArrayList<String> allWords = new ArrayList<>();
            allWords.addAll(angerWords);
            allWords.addAll(disgustWords);
            allWords.addAll(fearWords);
            allWords.addAll(joyWords);
            allWords.addAll(sadnessWords);
            allWords.addAll(surpriseWords);
            if(allWords.size()>0)
                mongo.writeTagsForTotalTime(trend, allWords);
            else
            	System.out.println("WARNING: trend without any frequent words found. No tags created for trend "+trend);
          	finalAngerScore.clear();
          	finalDisgustScore.clear();
          	finalFearScore.clear();
          	finalJoyScore.clear();
          	finalSadnessScore.clear();
          	finalSurpriseScore.clear();
    	}
    }
    
    private static String[] tokenizeTweet(String tweetText)
    {
    	Tokenizer tokenizer = new Tokenizer(tweetText);
    	tokenizer.tokenize();
    	String[] words = tokenizer.getWords();
    	return words;
    }
    
    private static int findStrongestSentiment(double[] sentiments)
    {
    	double max = sentiments[0];
    	int strongestSentimentId = 0;
    	for(int i=1;i<6;++i)
    	{
    		if(sentiments[i] > max)
    		{
    			strongestSentimentId = i;
    			max = sentiments[i];
    		}
    	}
    	return strongestSentimentId;
    }
    
    private static void writeToHashTables(int id,String word)// int id is the id of sentiment e.g (0 for anger, 1 for disgust etc.)
    {
    	if(id == 0)
    	{
    		if(finalAngerScore.containsKey(word))
    		{
    			finalAngerScore.put(word, finalAngerScore.get(word)+1);
    		}
    		else
    		{
    			finalAngerScore.put(word, 1);
    		}
    		if(angerScore.containsKey(word))
    		{
    			angerScore.put(word, angerScore.get(word)+1);
    		}
    		else
    		{
    			angerScore.put(word, 1);
    		}
    	}
    	else if(id == 1)
    	{
    		if(finalDisgustScore.containsKey(word))
    		{
    			finalDisgustScore.put(word, finalDisgustScore.get(word)+1);
    		}
    		else
    		{
    			finalDisgustScore.put(word, 1);
    			
    		}
    		if(disgustScore.containsKey(word))
    		{
    			disgustScore.put(word, disgustScore.get(word)+1);
    		}
    		else
    		{
    			disgustScore.put(word, 1);
    		}
    	}
    	else if(id == 2)
    	{
    		if(finalFearScore.containsKey(word))
    		{
    			finalFearScore.put(word, finalFearScore.get(word)+1);
    		}
    		else
    		{
    			finalFearScore.put(word, 1);
    		}
    		if(fearScore.containsKey(word))
    		{
    			fearScore.put(word, fearScore.get(word)+1);
    		}
    		else
    		{
    			fearScore.put(word, 1);
    		}
    	}
    	else if(id == 3)
    	{
    		if(finalJoyScore.containsKey(word))
    		{
    			finalJoyScore.put(word, finalJoyScore.get(word)+1);
    		}
    		else
    		{
    			finalJoyScore.put(word, 1);
    		}
    		if(joyScore.containsKey(word))
    		{
    			joyScore.put(word, joyScore.get(word)+1);
    		}
    		else
    		{
    			joyScore.put(word, 1);
    		}
    	}
    	else if(id == 4)
    	{
    		if(finalSadnessScore.containsKey(word))
    		{
    			finalSadnessScore.put(word, finalSadnessScore.get(word)+1);
    		}
    		else
    		{
    			finalSadnessScore.put(word, 1);
    		}
    		if(sadnessScore.containsKey(word))
    		{
    			sadnessScore.put(word, sadnessScore.get(word)+1);
    		}
    		else
    		{
    			sadnessScore.put(word, 1);
    		}
    	}
    	else if(id == 5)
    	{
    		if(finalSurpriseScore.containsKey(word))
    		{
    			finalSurpriseScore.put(word, finalSurpriseScore.get(word)+1);
    		}
    		else
    		{
    			finalSurpriseScore.put(word, 1);
    		}
    		if(surpriseScore.containsKey(word))
    		{
    			surpriseScore.put(word, surpriseScore.get(word)+1);
    		}
    		else
    		{
    			surpriseScore.put(word, 1);
    		}
    	}
    }
    
    private static double differenceInSeconds(Date earlierDate, Date laterDate)
    {
    	try
    	{
    		double result = ((laterDate.getTime()/1000.0) - (earlierDate.getTime()/1000.0));
        	//System.out.println(result);
        	return result;
    	}
    	catch(NullPointerException e)
    	{
    		System.out.println("WARNING: null Date found in tweetObject. Proccess returns 0 as difference in minutes between the two tweets.");
    		return 0.0;
    	}
    }
    
    private static ArrayList<String> findTopWords(Hashtable<String,Integer> words)
    {
    	ArrayList<String> topWords = new ArrayList<>();
    	String maxKey="";
    	int maxValue = Integer.MIN_VALUE;
    	for(int i=0;i<5;++i)
    	{
    		for(Map.Entry<String,Integer> entry : words.entrySet())
        	{
        	     if(entry.getValue() > maxValue) 
        	     {
        	         maxValue = entry.getValue();
        	         maxKey = entry.getKey();
        	     }
        	}
    		words.remove(maxKey);
    		maxValue = Integer.MIN_VALUE;
    		topWords.add(maxKey);
    	}
    	return topWords;
    }
    
    private static Hashtable<String,Integer> getTopSentimentHashTable(int id)
    {
    	Hashtable<String,Integer> table = new Hashtable<>();
    	if(id ==0)
    	{
    		table = angerScore;
    	}
    	else if(id == 1)
    	{
    		table = disgustScore;
    	}
    	else if(id == 2)
    	{
    		table = fearScore;
    	}
    	else if(id == 3)
    	{
    		table = joyScore;
    	}
    	else if(id == 4)
    	{
    		table = sadnessScore;
    	}
    	else if(id == 5)
    	{
    		table = surpriseScore;
    	}
    	return table;
    }
    
}

