package SentimentAnalysis;

/*Class for tokenization of strings to an array of words*/

public class Tokenizer{

	String[] words;
	String line;
	
	public Tokenizer(String line) 
	{
		this.line = line; 
	}

	public void tokenize()
	{
		String[] words = line.split("\\W+");// We split the line based on universal split characters.
		this.words = words;
	}
	
	public String[] getWords()
	{
		return words;
	}

}

